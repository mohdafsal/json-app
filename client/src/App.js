import React, { useState, useEffect } from "react";
import "./App.css";
import Button from "./components/Button/index";
import Container from "./components/Container/index";
import { Provider } from "react-redux";
import store from "./store/store";
import axios from "axios";

const App = () => {
  const [loggedIn, setLoggedIn] = useState(true);

  useEffect(() => {
    const params = new URL(window.location).searchParams;
    const cookie = document.cookie;
    const code = params.get("code");
    if (code && cookie.includes("token")) {
      setLoggedIn(true);
    } else if (code) {
      const finalURL = ``;
      axios
        .post(finalURL)
        .then(res => {
          const { access_token, expires_in } = res.data;
          document.cookie = `token=${access_token}; max-age=${expires_in}`;
          setLoggedIn(true);
        })
        .catch(err => console.log("err", err));
    }
  }, []);

  useEffect(() => {
    axios.get("/api/hello").then(res => console.log("res", res)).catch(err => console.error(err));
  }, []);

  return (
    <Provider store={store}>
      {!!loggedIn && (
        <div className="App">
          <Button />
          <Container />
        </div>
      )}
    </Provider>
  );
};

export default App;