import React from "react";

const DataCard = ({
  data,
  openPopUp
}) => {
  return (
    <div className="card">  
      <h2 className="card__title">
        {data.title || ""}
      </h2>
      <div className="card-body">
        <p>id: {data.id || ""}</p>
      </div>
      <button 
        onClick={() => openPopUp(data)}
        className="card__read-more"
      >
        READ MORE
      </button>
    </div>
  )
}

export default DataCard;