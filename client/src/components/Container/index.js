import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import PopUp from "../PopUp";
import DataCard from "../DataCard/index";
import uuid from "uuid4";

const Container = ({data}) => {
const [popUpData, setPopUpData] = useState([]);
const [popUp, setPopUp] = useState(false);

const openPopUp = (data) => {
  setPopUpData(data);
  setPopUp(true);
  document.body.classList.add("hide-scroll");
}

const closePopUp = () => {
  setPopUp(false);
  document.body.classList.remove("hide-scroll");
}

 const loadDataCards = (datas) => { 
   let renderData = [];
   for (let data in datas) {
    const html = (<DataCard 
      key={uuid()} 
      data={datas[data]} 
      openPopUp={openPopUp}
    />)
      renderData.push(html);
   }
   return renderData;
 } 

  return (
    <>
    <main className="main">
        {data ? loadDataCards(data) : <p className="loading-text">Loading...</p>}
    </main>  
    {popUp && (
      <PopUp 
        popUpData={popUpData}
        closePopUp={closePopUp}
      />
    )}
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    data: state.data.updatedData
  } 
}

export default connect(mapStateToProps)(Container);