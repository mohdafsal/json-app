import React, {useState, useEffect} from "react";
import {JsonEditor as Editor} from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';
import {useToasts} from 'react-toast-notifications';
import {connect} from "react-redux";
import {
  updateData,
  deleteData
} from "../../store/data/actions";
import axios from "axios";

const PopUp = ({
  popUpData,
  closePopUp,
  dispatchUpdatedData,
  dispatchDeletedData
}) => {
  const [data, setData] = useState({});
  const { addToast: triggerToast } = useToasts();

  useEffect(() => {
    setData(popUpData);
  }, []);


  if (!Object.keys(popUpData)) {
    return null;
  } 


  const getCookie = (name) => {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  const updateDataHandler = () => {
    const {id} = data;
    const payload = {id};
    const token = getCookie("token");
    if (!token) {
      triggerToast("token is not available", {
        appearance: 'error',
        autoDismiss: true,
      })
      return;
    }
    const config = {
      headers: {
        "Content-type": "application/json",
        "token": token
      }
    }
    // call api for update
    const updateFunc = async () => {
      try {
        const finalURL = `https://apiurl.com/${id}`;
        const res = await axios.put(finalURL, payload, config);
        console.log("res", res);

        /* this dispatch function is only for updating the ui */
        dispatchUpdatedData(payload);
        triggerToast("Successfully updated", {
          appearance: 'success',
          autoDismiss: true,
        })
      } catch (err) {
        console.log("err", err);
        triggerToast("Something went wrong", {
          appearance: 'error',
          autoDismiss: true,
        })
        return;
      }
    } 

    updateFunc();
  }

  const deleteDataHandler = () => {
    const {id} = data;
    dispatchDeletedData(id);
    const token = getCookie("token");
    if (!token) {
      triggerToast("token is not available", {
        appearance: 'error',
        autoDismiss: true,
      })
      return;
    }

    const config = {
      headers: {
        "Content-type": "application/json",
        "token": token
      }
    }
    
    // call api for update
    const deleteFunc = async () => {
      try {
        const finalURL = `https://apiurl.com/${id}`;
        const res = await axios.delete(finalURL, config);
        /* this dispatch function is only for updating the ui */
        closePopUp();
        triggerToast("Successfully deleted", {
          appearance: 'success',
          autoDismiss: true,
        })
      } catch (err) {
        console.log("err", err);
        triggerToast("Something went wrong", {
          appearance: 'error',
          autoDismiss: true,
        })
        return;
      }
    } 

    deleteFunc();
  }

  const handleChange = (updatedJSON) => {
    setData(updatedJSON)
  }

  return (
    <div className="popup-container">
       <div className="popup-content">
         {!!Object.keys(data).length && ( 
          <Editor
              value={data}
              onChange={handleChange}
          />
         )}
        </div> 
        <div className="popup-buttons">
          <button   
            className="popup-buttons__btn green"
            onClick={updateDataHandler}
          >
            UPDATE
          </button>
          <button 
            className="popup-buttons__btn red"
            onClick={deleteDataHandler}
          >
            DELETE
          </button>
        </div>
        <div 
          onClick={closePopUp}
          className="close-btn" 
        />
    </div>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatchUpdatedData: data => {
    dispatch(updateData(data));
  },
  dispatchDeletedData: dataId => {
    dispatch(deleteData(dataId));
  }
});

export default connect(null, mapDispatchToProps)(PopUp);