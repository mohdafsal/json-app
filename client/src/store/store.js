import {createStore} from "redux";
import {combineReducers} from "redux";
import data from "./reducer";

const rootReducer = combineReducers({data});


const store = createStore(
  rootReducer
);

export default store;