import {
  ADD_DATA,
  UPDATE_DATA,
  DELETE_DATA
} from "../types";

const initialState = {};

export const updateReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DATA: 
      return {
        ...state,
        ...action.value
      }
    case UPDATE_DATA: 
      return {
        ...state,
        ...action.value
      }  
    case DELETE_DATA: 
      const id = action.value;
      const updatedState = {...state};
      delete updatedState[id];
      return updatedState;
    default: 
      return state;
  }
}
