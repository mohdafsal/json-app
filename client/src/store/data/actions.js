import {
   ADD_DATA,
   UPDATE_DATA,
   DELETE_DATA
} from "../types";

export const addData = (data) => {
 return { 
    type: ADD_DATA,
    value: data
 } 
}
export const updateData = (data) => {
 return { 
    type: UPDATE_DATA,
    value: data
 } 
}
export const deleteData = (data) => {
 return { 
    type: DELETE_DATA,
    value: data
 } 
}